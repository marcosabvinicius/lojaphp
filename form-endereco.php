<?php include("cabecalho.php");?>

<div class="container page-content form-register">
	
		<form class="form-horizontal"  method="post" action="adiciona-endereco.php">
				<div class="form-group"><h4>Endereço Principal</h4></div>
                
                <div class="form-group">
                    <label for="cep" class="col-sm-3 control-label">CEP</label>
                    <div class="col-sm-9">
                        <input type="cep" id="cep" name="cep"class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="rua" class="col-sm-3 control-label">Rua</label>
                    <div class="col-sm-9">
                        <input type="text" id="rua" name="rua" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="num" class="col-sm-3 control-label">Número</label>
                    <div class="col-sm-9">
                        <input type="text" id="num" name="num" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="complemento" class="col-sm-3 control-label">Complemento</label>
                    <div class="col-sm-9">
                        <input type="text" id="complemento" name="complemento" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="bairro" class="col-sm-3 control-label">Bairro</label>
                    <div class="col-sm-9">
                        <input type="text" id="bairro" name="bairro" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="estado" class="col-sm-3 control-label">Estado</label>
                    <div class="col-sm-9">
                        <select id="estado" name="estado" class="form-control">
                            <option value="">Selecione</option>
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amapá</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Ceará</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Espirito Santo</option>
							<option value="GO">Goiás</option>
							<option value="MA">Maranhão</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MT">Mato Grosso</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Pará</option>
							<option value="PB">Paraíba</option>
							<option value="PR">Paraná</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piauí</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rondônia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">São Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>
                        </select>
                    </div>
                </div> 
				<div class="form-group">
                    <label for="cidade" class="col-sm-3 control-label">Cidade</label>
                    <div class="col-sm-9">
                        <input type="text" id="cidade" name="cidade" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="referencia" class="col-sm-3 control-label">Referência</label>
                    <div class="col-sm-9">
                        <input type="text" id="referencia" name="referencia" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="tel1" class="col-sm-3 control-label">Telefone 1</label>
                    <div class="col-sm-9">
                        <input type="text" id="tel1" name="tel1" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="tel2" class="col-sm-3 control-label">Telefone 2</label>
                    <div class="col-sm-9">
                        <input type="text" id="tel2" name="tel2" class="form-control">
                    </div>
                </div>

				     <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                       <p> <input type="submit" name="cadastrar" value="Cadastrar" class="btn btn-info btnPesquisa"></p>
                    </div>
                </div>
            </form>
	</div>
				
<?php include("rodape.php");?>