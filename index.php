﻿
<?php include("cabecalho.php");
	  include("conexao.php");
      include("livro-lista.php"); 
?>

<table border = '1'>

    <div class="container page-content">
	
		<div class="row"> 
		
			<div class="row content-right">
				<div style="margin: 15px">
					<h2><b>Destaques</b></h2>
				</div>
			<?php
				$livros = listaLivros($conexao);
				foreach($livros as $livro) {
			?>
				<tr>
                <div class="col-sm-6 col-lg-6 col-md-6">
					<input type="hidden" id="idlivro" name="idlivro" value="<?=$livro['IDLIVRO']?>" />
                    <div class="thumbnail product-box">
                        <img src=<?= $livro['PATHIMAGEM']?> alt="">
						<div class="caption">
							<h5 class="pull-right">R$<?= $livro['PRECO'] ?></h5>
                            <h5><a href="detalhe.php?idlivro=<?=$livro['IDLIVRO']?>"><?= $livro['TITULO'] ?></a></h5>
                        </div>
                    </div>
                </div>
            </div>		
		</div>
	
	</div>
</tr>
	</table>
<?php
	}//finalizando o for each do script php
?>
	
	<?php
		 include("rodape.php"); 
	?>