<?php
include("cabecalho.php");
?>
	<div class="container page-content form-register">
		<form class="form-horizontal"  method="post" action="adiciona-cliente.php">
				<div class="form-group"><h4>QUERO ME CADASTRAR</h4></div>
                <div class="form-group">
                    <label for="nome" class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-9">
                        <input type="text" id="nome" name="nome" class="form-control" autofocus>
                    </div>
                </div>
				<div class="form-group">
                    <label for="sobrenome" class="col-sm-3 control-label">Sobrenome</label>
                    <div class="col-sm-9">
                        <input type="text" id="sobrenome"  name="sobrenome"class="form-control" autofocus>
                    </div>
                </div>
				<div class="form-group">
                    <label for="cpf" class="col-sm-3 control-label">CPF</label>
                    <div class="col-sm-9">
                        <input type="text" id="cpf" name="cpf" class="form-control" autofocus>
                    </div>
                </div>
				<div class="form-group">
                    <label for="telefone" class="col-sm-3 control-label">Telefone</label>
                    <div class="col-sm-9">
                        <input type="text" id="telefone" name="telefone"class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">E-mail</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="sexo" class="col-sm-3 control-label">Sexo</label>
                    <div class="col-sm-9">
                        <select id="sexo" name="sexo" class="form-control">
                            <option>Masculino</option>
                            <option>Feminino</option>
                        </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="datanasc" class="col-sm-3 control-label">Nascimento</label>
                    <div class="col-sm-9">
                        <input type="date" id="datanasc" name="datanasc" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="senha" class="col-sm-3 control-label">Senha</label>
                    <div class="col-sm-9">
                        <input type="password" id="senha" name="senha" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
						<p> <input type="submit" name="cadastrar" value="Cadastrar" class="btn btn-info btnPesquisa"></p>
                    </div>
                </div>
            </form>
	</div>
<?php include("rodape.php"); ?>