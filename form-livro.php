﻿<?php include("cabecalho-admin.php");?>

<div class="container page-content form-register">
	
		<form class="form-horizontal"  method="post" action="adiciona-livro.php">
				<div class="form-group"><h4>Cadastro de Livro</h4></div>
                
                <div class="form-group">
                    <label for="titulo" class="col-sm-3 control-label">Título</label>
                    <div class="col-sm-7">
                        <input type="text" id="titulo" name="titulo" class="form-control">
                    </div>
                </div>
		<div class="form-group">
                    <label for="descricao" class="col-sm-3 control-label">Descrição</label>
                    <div class="col-sm-7">
                        <input type="text" id="descricao" name="descricao" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="edicao" class="col-sm-3 control-label">Edição</label>
                    <div class="col-sm-7">
                        <input type="text" id="edicao" name="edicao" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="qtdepaginas" class="col-sm-3 control-label">Quantidade de Páginas</label>
                    <div class="col-sm-7">
                        <input type="text" id="qtdepaginas" name="qtdepaginas" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="editora" class="col-sm-3 control-label">Editora</label>
                    <div class="col-sm-7">
                        <input type="text" id="editora" name="editora" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="genero" class="col-sm-3 control-label">Gênero</label>
                    <div class="col-sm-7">
                        <select id="genero" name="genero" class="form-control">
                            <option value="">Selecione</option>
							<option value="acao">Ação</option>
							<option value="suspense">Suspense</option>
							<option value="fantasia">Fantasia</option>
							<option value="artes">Artes</option>
							<option value="didaticos">Didáticos</option>
							<option value="direito">Direito</option>
							<option value="economia">Economia</option>
							<option value="religiao">Religião</option>
                        </select>
                    </div>
                </div> 
				
				<div class="form-group">
                    <label for="qtde" class="col-sm-3 control-label">Quantidade de Exemplares</label>
                    <div class="col-sm-7">
                        <input type="text" id="qtde" name="qtde" class="form-control">
                    </div>
                </div>
				
		<div class="form-group">
                    <label for="pathimagem" class="col-sm-3 control-label">Caminho da Imagem</label>
                    <div class="col-sm-7">
                        <input type="text" id="pathimagem" name="pathimagem" class="form-control">
                    </div>
                </div> 
		
		<div class="form-group">
                    <label for="preco" class="col-sm-3 control-label">Preço</label>
                    <div class="col-sm-7">
                        <input type="text" id="preco" name="preco" class="form-control">
                    </div>
        </div>
		
		<div class="form-group">
                    <div class="col-sm-7 col-sm-offset-3">
                       <p> <input type="submit" name="cadastrar" value="Cadastrar" class="btn btn-info btnPesquisa"></p>
                    </div>
                </div>
            </form>
	</div>
				
<?php include("rodape-admin.php");?>