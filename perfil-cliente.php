<?php include("conexao.php"); ?>
<?php include("cabecalho.php"); ?>  
<?php include("busca-cliente.php"); ?>

<?php
	$cpf = $_GET['cpf'];
	$cliente =  buscaCliente($conexao, $cpf);
?>

<div class="container page-content">
	
	<table class="table table-striped table-bordered">
		<tr>
			<td>Nome: </td>
			<td><?=$cliente['NOME']?></td>
		</tr>
		<tr>
			<td>Sobrenome: </td>
			<td><?=$cliente['SOBRENOME']?></td>
		</tr>
		<tr>
			<td>Email: </td>
			<td><?=$cliente['EMAIL']?></td>
		</tr>
		<tr>
			<td>Senha: </td>
			<td><?=$cliente['SENHA']?></td>
		</tr>
		<tr>
			<td>CPF: </td>
			<td><?=$cliente['CPF']?></td>
		</tr>
		<tr>
			<td>Sexo: </td>
			<td><?=$cliente['SEXO']?></td>
		</tr>
		<tr>
			<td>Telefone: </td>
			<td><?=$cliente['TELEFONE']?></td>
		</tr>
		<tr>
			<td>Nascimento: </td>
			<td><?=$cliente['DATANASC']?></td>
		</tr>
	</table>
		
</div>
	
<?php include("rodape.php"); ?>