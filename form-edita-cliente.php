<?php
include("cabecalho.php");
?>

<?php
	$usuario =  buscaUsuario($conexao, $idusuario);
?>
	<div class="container page-content form-register">
		<form class="form-horizontal"  method="post" action="altera-cliente.php">
				<div class="form-group"><h4>EDITAR MEU CADASTRO</h4></div>
				<input type="hidden" id="idusuario" name="idusuario" value="<?=$usuario['IDUSUARIO']?>" />
                <div class="form-group">
                    <label for="nome" class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-9">
                        <input type="text" id="nome" name="nome" value="<?=$usuario['NOME']?>" class="form-control" autofocus>
                    </div>
                </div>
				<div class="form-group">
                    <label for="sobrenome" class="col-sm-3 control-label">Sobrenome</label>
                    <div class="col-sm-9">
                        <input type="text" id="sobrenome"  name="sobrenome" value="<?=$usuario['SOBRENOME']?>" class="form-control" autofocus>
                    </div>
                </div>
				<div class="form-group">
                    <label for="cpf" class="col-sm-3 control-label">CPF</label>
                    <div class="col-sm-9">
                        <input type="text" id="cpf" name="cpf" value="<?=$usuario['CPF']?>" class="form-control" autofocus>
                    </div>
                </div>
				<div class="form-group">
                    <label for="telefone" class="col-sm-3 control-label">Telefone</label>
                    <div class="col-sm-9">
                        <input type="text" id="telefone" name="telefone" value="<?=$usuario['TELEFONE']?>" class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">E-mail</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" value="<?=$usuario['EMAIL']?>" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="sexo" class="col-sm-3 control-label">Sexo</label>
                    <div class="col-sm-9">
                        <select id="sexo" name="sexo" value="<?=$usuario['SEXO']?>" class="form-control">
                            <option>Masculino</option>
                            <option>Feminino</option>
                        </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="datanasc" class="col-sm-3 control-label">Nascimento</label>
                    <div class="col-sm-9">
                        <input type="date" id="datanasc" name="datanasc" value="<?=$usuario['DATANASC']?>" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label for="senha" class="col-sm-3 control-label">Senha</label>
                    <div class="col-sm-9">
                        <input type="password" id="senha" name="senha" value="<?=$usuario['SENHA']?>"class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
						<p> <input type="submit" name="cadastrar" value="Alterar" class="btn btn-info btnPesquisa"></p>
                    </div>
                </div>
            </form>
	</div>
<?php include("rodape.php"); ?>