<?php include("cabecalho-admin.php"); ?>

<div class="container page-content">

		<div class="row" style="margin-top:20px">
			<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form role="form">
					<fieldset>
						<h2>Login</h2>
						<div class="form-group">
							<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Login ou E-mail">
						</div>
						<div class="form-group">
							<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Senha">
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<a href="livro-table.php" class="btn btn-lg btn-success btn-block">Entrar</a>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
		
	</div>
	
<?php include("rodape-admin.php"); ?>
