<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>Loja do Marcos</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>
	<nav class="navbar navbar-default navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span> 
		  </button>
		  <a class="navbar-brand" href="index.php">Loja do Marcos</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="pagamento.html"><span style="font-size:20px; margin-right:25px" class="glyphicon glyphicon-shopping-cart"></span></a></li>
			<li class="dropdown">
                <button class="btn btn-primary dropdown-toggle accBtn" type="button" data-toggle="dropdown" >Opções</button>
                <ul class="dropdown-menu">
                    <li><a href="perfil-usuario.php">Perfil</a></li>
                    <li><a href="meus_pedidos.html">Meus Pedidos</a></li>
                    <li><a href="form-cliente.php">Quero me cadastrar ! </a></li>
					<li><a href="form-endereco.php">Cadastrar Endereço</a></li>
					<li><a href="form-livro.php">Funcionários </a></li>
                </ul>
            </li>
		  </ul>
		</div>
	  </div>
	</nav>