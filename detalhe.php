<?php include("conexao.php"); ?>
<?php include("cabecalho.php"); ?>  
<?php include("busca-livro.php"); ?>

<?php
	$livro =  buscaLivro($conexao, $idlivro);
?>

<div class="container page-content">
	
		<div class="row">
			<div class="col-xs-4 item-photo">
                <img style="max-width:100%;" src="" />
            </div>
            <div class="col-xs-5" style="border:0px solid gray">

                <h3><?=$livro['TITULO']?></h3>    
                <img src=<?=$livro['PATHIMAGEM']?> />
                <h6 class="title-price"></h6>
                <h3 style="margin-top:0px;">R$ <?=$livro['PRECO']?></h3>
				<div class="section" style="padding-bottom:20px;">
                    <h6 class="title-attr"><small>Descrição</small></h6>                    
                    <div>
						<?=$livro['DESCRICAO']?>
                    </div>
                </div>    
                <div class="section" style="padding-bottom:20px;">
                    <h6 class="title-attr"><small>Quantidade</small></h6>                    
                    <div>
                        <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
                        <input value="1" />
                        <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
                    </div>
                </div>                

                <div class="section" style="padding-bottom:20px;">
                    <button class="btn btn-success"><span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Adicionar ao Carrinho</button>
                </div>                                        
            </div>                              	
		</div>
		
	</div>
	
<?php include("rodape.php"); ?>