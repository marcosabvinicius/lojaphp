
<?php include("cabecalho-admin.php"); ?>
<?php include("conexao.php"); ?>
<?php include("livro-lista.php"); ?>


<?php
	$livros = listaLivros($conexao);
?>

<?php if(array_key_exists("removido", $_GET) && $_GET['removido']=='true') { ?>
	<p class="alert-success">Produto apagado com sucesso.</p>
<?php 
	}
?>

	<table class="table table-striped table-bordered">

		<tr>
			<th>Código</th>
			<th>Título</th>
			<th>Quantidade</th> 
			<th>Gênero</th>
			<th>Editar</th> 
			<th>Excluir</th>
		</tr>
	
<?php
	foreach($livros as $livro) {
?>

    <tr>
		<td><?= $livro['IDLIVRO'] ?></td>
        <td><?= $livro['TITULO'] ?></td>
        <td><?= $livro['QTDE'] ?></td>
		<td><?= $livro['GENERO']?></td>
		<td><a href = "form-edita-livro.php?idlivro=<?= $livro['IDLIVRO'] ?>"> Editar </a>
		<td><a href = "remove-livro.php?idlivro=<?= $livro['IDLIVRO'] ?>"> Excluir </a>
	</tr>

<?php
	}//finalizando o for each do script php
?>
</table>

<?php include("rodape-admin.php"); ?>