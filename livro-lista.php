
<?php include("conexao.php"); ?>

<?php

	function listaLivros($conexao) {
		$livros = array();
		//colocado o resultado do select na variavel $resultado
		$resultado = mysqli_query($conexao, "select * from livros");

		//pegando a lista trazida do banco
		while($livro = mysqli_fetch_assoc($resultado)) {
			array_push($livros, $livro);
		}
		return $livros;
	}

?>
